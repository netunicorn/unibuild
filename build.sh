#! /bin/sh

if [ "$2" = "" ]; then
    echo "Usage: $0 <output> <input>"
    exit 1
fi

exec cc -m64 -static -Wall -fno-builtin -fno-exceptions -fno-stack-protector -nodefaultlibs -nostartfiles -fno-pie -T ./binary.ld \
-o "$1" ./stdlib.c $2
