#include <stdint.h>
#include <stddef.h>

enum {
    CMD_EXIT,
    CMD_MEMINFO,
    CMD_HEAP_ENABLE,
    CMD_HEAP_DISABLE,
    CMD_PRINT
};

struct mem_info {
    uint64_t executable_adress, stack_adress, heap_adress;
    size_t executable_size, stack_size, heap_size;
} __attribute__((__packed__));

typedef uint64_t cmd_t;
