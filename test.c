#include "stdlib.h"


int main() {
    struct mem_info meminfo;
    get_mem_info(&meminfo);
    printf("Hello world!\n"
           "Stack addr: %p\n"
           "Heap addr: %p\n"
           "Test int: %i\n", meminfo.stack_adress, meminfo.heap_adress, 1234);

    print("Enabling heap... ");
    enable_heap();
    print("Done\n");
    printf("Currently used memory: %lB\n", get_mem_used());
    print("Trying to copy string into heap... ");
    char *test = malloc(6);
    strcpy(test, "Done\n");
    print(test);
    printf("Currently used memory: %lB\n", get_mem_used());
    print("Freeing memory back up... ");
    free(test);
    print("Done\n");
    printf("Currently used memory: %lB\n", get_mem_used());
}
