#include "stdlib.h"



// Host communication
cmd_t send_command(cmd_t id, cmd_t data, cmd_t size) {
    cmd_t fres;
    asm volatile ("syscall" : "=a"(fres) : "a"(id), "b"(data), "d"(size) : "rcx", "r11", "memory");
    return fres;
}

void get_mem_info(struct mem_info *dest) {
    send_command(CMD_MEMINFO, (cmd_t)dest, 0);
}

void exit() {
    send_command(CMD_EXIT, 0, 0);
}


// I/O
static char printbuf[32];
static size_t printbuf_pos = 0;

void printbuf_flush() {
    if (printbuf_pos != 0) {
        size_t pos_bak = printbuf_pos;
        printbuf_pos = 0;
        print_n(printbuf, pos_bak);
    }
}

void printbuf_check() {
    if (printbuf_pos == sizeof(printbuf)) {
        printbuf_flush();
    }
}

void print(const char *str) {
    printbuf_flush();
    send_command(CMD_PRINT, (cmd_t)str, (cmd_t)strlen(str));
}

void print_n(const char *str, size_t n) {
    printbuf_flush();
    send_command(CMD_PRINT, (cmd_t)str, (cmd_t)n);
}

void putchar(char character) {
    printbuf[printbuf_pos++] = character;
    printbuf_check();
}

void print_hex_impl(uint64_t num, int nibbles, _Bool padding) {
    // Check if number is 0
    if (num == 0 && !padding) {
        putchar('0');
        return;
    }
    // Start printing
    _Bool ispadding = !padding;
    for(int i = nibbles - 1; i >= 0; -- i) {
        char out = "0123456789ABCDEF"[(num >> (i * 4))&0xF];
        // If number isn't 0, this isn't padding
        if (out != '0') {
            ispadding = 0;
        }
        // If no more padding, print
        if (!ispadding) {
            putchar(out);
        }
    }
}

void print_dec(uint64_t num) {
    // Output string
    char numstr[21]; // 20 is maximum amount of digits in num
    numstr[sizeof(numstr) - 1] = '\0'; // Has to be null-terminated
    char *numstart = numstr + sizeof(numstr) - 2;
    // Check if number is signed
    if (num == 0) {
        putchar('0');
        return;
    }
    // Convert to string
    while (num != 0) {
      *(numstart--) = num % 10 + '0';
      num = num / 10;
    }
    // Print result
    print(numstart + 1);
}

void printf(const char *format, ...) {
    va_list ap;
    _Bool nexteval = 0;
    va_start(ap, format);
    for (const char *thischar = format; *thischar != '\0'; thischar++) {
        if (*thischar == '%') {
            if (!(nexteval = !nexteval)) {
                putchar('%');
            }
        } else if (nexteval) {
            switch(*thischar) {
                case 's': print(va_arg(ap, const char *)); break;
                case 'c': putchar((const char)va_arg(ap, const int)); break;
                case 'p': print("0x"); case 'x': print_hex(va_arg(ap, uint64_t), 1); break;
                case 'd': case 'i': case 'l': print_dec(va_arg(ap, uint64_t)); break;
                default: print("<unknown type>");
            }
            nexteval = 0;
        } else {
            putchar(*thischar);
        }
    }
    va_end(ap);
}


// String utilities
size_t strlen(const char *str) {
    size_t len = 0;
    while (*str++)
        len++;
    return len;
}

void *memset(void *dst, int ch, size_t count) {
    for(size_t i = 0; i < count; i++)
        ((char*)dst)[i] = ch;
    return dst;
}

void *memcpy(void *dst, const void *src, size_t len) {
    for(size_t i = 0; i < len; i++)
        ((uint8_t*)dst)[i] = ((uint8_t*)src)[i];
    return dst;
}

char *strcpy(char *dest, const char *src) {
    memcpy(dest, src, strlen(src) + 1);
    return dest;
}

char *strncpy(char *dest, const char *src, size_t n) {
    size_t it;
    for (it = 0; it < n && src[it]; it++)
        dest[it] = src[it];
    for (; it < n; it++)
        dest[it] = 0;
    return dest;
}

char *strcat(char *dest, const char *src) {
    strncpy(dest + strlen(dest), src, strlen(src));
    return dest;
}


// Heap
struct memchunk_free {
    uint64_t size;
    struct memchunk_free *next;
};

static uint64_t mem_used = 0;
static struct memchunk_free heap = {.size = 0, .next = 0};
static struct mem_info meminfo;


void enable_heap() {
    send_command(CMD_HEAP_ENABLE, 0, 0);
    get_mem_info(&meminfo);
    heap.next = (struct memchunk_free *)meminfo.heap_adress;
    heap.next->size = meminfo.heap_size;
    heap.next->next = 0;
}

inline static uint64_t size_align(uint64_t num) {
    return ((num + sizeof(struct memchunk_free) - 1) / sizeof(struct memchunk_free)) * sizeof(struct memchunk_free);
}

void *heap_alloc(size_t *size) {
    // Allocations can't be smaller than this
    if(*size < sizeof(struct memchunk_free))
        *size = sizeof(struct memchunk_free);

    // Force-align size
    *size = size_align(*size);
    // Find first free space
    struct memchunk_free *nextmatch = heap.next, *baknext = &heap;

    // Get a chunk which fits this size
    while (nextmatch && nextmatch->size < *size) {
        baknext = nextmatch;
        nextmatch = nextmatch->next;
    }

    // If no chunk fits
    if(!nextmatch || nextmatch->size < *size) {
        return (void*)-1;
    }

    // Calculate the remaining size of the chunk
    uintptr_t new_size = nextmatch->size - *size;

    // If there is anything left, place it back in the list
    if(new_size >= sizeof(struct memchunk_free)) {
        baknext->next = (struct memchunk_free *)((uintptr_t)nextmatch + *size);
        baknext->next->size = new_size;
    } else {
        *size = nextmatch->size;
        baknext->next = nextmatch->next;
    }

    // Increase counter
    mem_used += *size;

    // Return allocated memory
    return nextmatch;
}

void *malloc(size_t size) {
    // Add space for size
    size += sizeof(uint64_t);
    uint64_t *mem = (uint64_t *)heap_alloc(&size);
    mem[0] = size;
    return mem + 1;
}

void heap_dealloc(void *memptr, size_t size) {
    // Convert this back into a chunk
    struct memchunk_free *chunk = (struct memchunk_free *)memptr;

    // Reinsert the chunk into the freelist
    chunk->size = size;
    chunk->next = heap.next;
    heap.next = chunk;

    // Decrease counter
    mem_used -= size;
}

void free(void *memptr) {
    uint64_t *mem = (uint64_t *)memptr - 1;
    heap_dealloc(mem, mem[0]);
}

size_t get_mem_used() {
    return mem_used;
}
size_t get_mem_total() {
    return meminfo.heap_size;
}
size_t get_mem_free() {
    return get_mem_total() - get_mem_used();
}


// Entry point
int main();
__attribute__((section(".start")))
void _start() {
    main();
    printbuf_flush();
    exit();
}
