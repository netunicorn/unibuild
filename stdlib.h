#include <stdint.h>
#include <stdarg.h>
#include "api.h"
#define print_hex(num, padding) print_hex_impl((num), sizeof((num)) * 2, padding)

typedef uint64_t size_t;


// Host communication
cmd_t send_command(cmd_t id, cmd_t data, cmd_t size);
void get_mem_info(struct mem_info *dest);
void exit();

// I/O
void printbuf_flush();
void printbuf_check();
void print(const char *str);
void print_n(const char *str, size_t n);
void putchar(char character);
void print_hex_impl(uint64_t num, int nibbles, _Bool padding);
void print_dec(uint64_t num);
void printf(const char *format, ...);

// String utilities
size_t strlen(const char *str);
void *memset(void *dst, int ch, size_t count);
void *memcpy(void *dst, const void *src, size_t len);
char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, size_t n);
char *strcat(char *dest, const char *src);

// Heap
void enable_heap();
void *heap_alloc(size_t *size);
void *malloc(size_t size);
void heap_dealloc(void *memptr, size_t size);
void free(void *memptr);
size_t get_mem_used();
size_t get_mem_total();
size_t get_mem_free();
